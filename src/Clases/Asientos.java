package Clases;

public class Asientos {
    //clase asientos para manejar los campos en la sala de cine
    
    private static String [][] asientos = new String[8][9];//matriz de 8*9 para la identificacion de los asientos
    private static boolean [][] asientosComprados = new boolean[8][9];//matriz de 8*9 para asignar los asientos comprados
    private static boolean unico = false;//boolean para realizar el constructor solo una vez
    
    public Asientos(){
        //método constructor de la clase Asientos
        if(!unico){//si aún no se ha creado la sala
            llenarAsientos();
            randomizarAsientos();
            unico = true;
        }
    }
    private void llenarAsientos(){
        //método para llenar los asientos con su respectiva identificación 
        int character;
        int num = 8;
        char letra;
        String asiento;
        for (int f = 0; f < 8; f++) {
            character = 65;
            for (int c = 0; c < 9; c++) {
                letra = (char)character;
                asiento = String.valueOf(num);
                asiento = asiento.concat(String.valueOf(letra));
                asientos[f][c] = asiento;
        
                character++;
            }
            num--;
        }
    }
    public void mostrarMatriz(){
        //método para mostrar la matriz de booleans de asientos comprados
        for (int f = 0; f < 8; f++) {
            for (int c = 0; c < 9; c++) {
              System.out.print(asientosComprados[f][c]+" ");
            }
            System.out.println("");
        }
    }
    
    private void randomizarAsientos(){
        //método para establecer 25 asientos comprados al azar
        int f = 0;
        int c = 0;
        int conta = 0;
        while(conta < 25){
            f = (int) (Math.random() * 8);
            c = (int) (Math.random() * 9);
            if(!asientosComprados[f][c]){
                asientosComprados[f][c] = true;
                conta++;
            }
        }
    }
    
    public void marcarAsiento(int f, int c,boolean value){
        //metodo para establecer un asiento determinado como comprado o no
        asientosComprados[f][c] = value;
       
    }
    
    public Object[] getFila(int f){
        //función que retorna una fila determinada por parámetros
        Object[] fila = new Object[9];
        System.arraycopy(asientos[f], 0, fila, 0, 9);
        return fila;
    }
    

    public String[][] getAsientos() {
        //función que retorna la matriz de strings de los identificadores de asientos
        return asientos;
    }

    public boolean[][] getAsientosComprados() {
        //función que retorna la matriz de booleans (comprado o no comprado) de los asientos
        return asientosComprados;
    } 
      
    public boolean verificarAsiento(int f, int c){
        //función que retorna el estado en que se encuentra un asiento determinado por parámetros
        boolean asientoComprado = asientosComprados[f][c];       
        
        return asientoComprado;
    }
    
    public String getValueAt(int f, int c){
        //función que retorna el identificador de un asiento determinado por parámetros
        return asientos[f][c];
    }
   
      
}
