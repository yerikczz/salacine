package Clases;

public class ComprarAsientos {
    //clase para mantener los asientos a comprar
    private int f; 
    private int c;
    
    public ComprarAsientos(int f, int c){
        //método constructor de la clase Comprar Asientos 
        this.f = f;
        this.c = c;
    }

    //Métodos set&get
    public int getF() {
        return f;
    }

    public void setF(int f) {
        this.f = f;
    }

    public int getC() {
        return c;
    }

    public void setC(int c) {
        this.c = c;
    }
    
    
}
