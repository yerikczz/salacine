/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

public class Persona {
    //clase persona para almacenar los datos requeridos por cliente
    
    private int cedula;//entero que almacena la cedula del cliente
    private String nombre;//String que almacena el nombre del cliente
    private String genero;//String que almacena el género del cliente
    private boolean boleto=false;//boolean para determinar si la persona ya compró boletos
   
    public Persona(int cedula,String nombre,String genero){
        //método constructor de la clase Persona
        this.cedula=cedula;
        this.nombre=nombre;
        this.genero=genero;
    }
    
    //Métodos set&get
    public int getCedula() {
        return cedula;
    }

    public void setCedula(int cedula) {
        this.cedula = cedula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }
    
    public boolean isBoleto() {
        return boleto;
    }

    public void setBoleto(boolean boleto) {
        this.boleto = boleto;
    }
   
}
