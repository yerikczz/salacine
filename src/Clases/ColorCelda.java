package Clases;

import java.awt.Color;
import java.awt.Component;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

public class ColorCelda extends JTable{
    //clase para establecer colores a la JTable
    
    private Asientos asientos;

    public ColorCelda(Asientos asientos) {
        //método constructor de la clase ColorCelda
        this.asientos = asientos;
    }
    
    @Override
    public Component prepareRenderer(TableCellRenderer renderer, int row, int column){
        //sobreescritura del método de renderizado de la tabla, de modo que se pueda modificador a preferencia colores por celda
        
        Component componente = super.prepareRenderer(renderer, row, column);
      
        
        if(asientos.getAsientosComprados()[row][column]){
            componente.setForeground(Color.RED);
        }else{
            componente.setForeground(Color.BLACK);
        }
        
        return componente;
        
    }
   
    
}
