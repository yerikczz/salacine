package GUI;

import Clases.Persona;
import static GUI.RegistroPersonas.listaClientes;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableModel;

public class Reportes extends javax.swing.JPanel {
    private static ArrayList<String> datos;
    private DefaultTableModel listModel;//Modelo para modificar la tabla de los asientos del cine
    public Reportes() {
        this.datos = RealizarCompra.facturas;
        initComponents();
        listModel = (DefaultTableModel) tblFacturas.getModel();
        initTable();
    }

    public void initTable(){
        String array[];
        String cedula, nombre,genero,asientos,monto,fecha;
        String linea;
        try {
            //try para manejar excepciones de apertura del archivo
            FileReader archivo = new FileReader("src/Archivos/facturas.txt");//Apertura del archivo en modo lectura
            BufferedReader buffer = new BufferedReader(archivo);//Creación del buffer para lectura del archivo
            try {
                //try para manejar excepciones de lectura de líneas
                while((linea=buffer.readLine())!=null){
                    //ciclo while que se ejecuta mientras la linea del archivo contenga algún caracter

                    array = linea.split(",");//separación de la línea por comas, asignando cada elemento a una posicion del arreglo diferente

                    System.out.println(Arrays.toString(array));
                    //asignación de los atributos del cliente obtenidos de la linea leída
                    cedula=array[0];
                    nombre=array[1];
                    genero=array[2];
                    asientos=array[3];
                    monto=array[4];
                    fecha=array[5];
                    
                   datos.add(linea);
                   listModel.addRow(new Object[]{cedula,nombre,genero,asientos,monto,fecha});
                }

                buffer.close();//cierre de buffer de lectura

            } catch (IOException ex) {
                System.out.println(ex);
            }          

        } catch (FileNotFoundException ex) {
            System.out.println(ex);
        }
    }
   
    public void guardarDatos(){
           
        FileWriter archivo;                  
        try {
            archivo = new FileWriter("src/Archivos/facturas.txt");
      
            //Se recorre la lista de personas y se escriben en el archivo
            for(String s: datos){
                archivo.write(s+"\n");
            }

            archivo.close();
        } catch (IOException ex) {
            Logger.getLogger(Reportes.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        tblFacturas = new javax.swing.JTable();

        tblFacturas.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Cedula", "Nombre", "Genero", "Asientos", "Monto", "Fecha"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, true, false, true, true
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(tblFacturas);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 375, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(15, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(14, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 354, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(32, 32, 32))
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tblFacturas;
    // End of variables declaration//GEN-END:variables
}
