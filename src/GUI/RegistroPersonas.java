/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import Clases.Persona;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import javax.swing.JOptionPane;


public class RegistroPersonas extends javax.swing.JPanel {
    //JPanel para registrar personas en el sistema
    
    public static ArrayList<Persona> listaClientes = new ArrayList();//ArrayList que contiene los clientes registrados
    private String[] atributosPersona;//Lista de Strings para almacenar los atributos del cliente en el momento de leer el archivo 
    
    //atributos del cliente
    private int cedula;
    private String nombre, genero;
    
    private static boolean unico = false;
    
    public RegistroPersonas() {
        //metodo constructor del JPanel
        
        initComponents();//método predefinido para inicializar componentes
        
        if(!unico){
            unico = true;          
            this.jRadioButtonF.setActionCommand("F");//asignación de comando de acción F al radioButton correspondiente para futura obtención 
            this.jRadioButtonM.setActionCommand("M");//asignación de comando de acción M al radioButton correspondiente para futura obtención

            String linea;//String que contendrá la línea leída del archivo
            try {
                //try para manejar excepciones de apertura del archivo

                FileReader archivo = new FileReader("src/Archivos/clientes.txt");//Apertura del archivo en modo lectura
                BufferedReader buffer = new BufferedReader(archivo);//Creación del buffer para lectura del archivo

                try {
                    //try para manejar excepciones de lectura de líneas

                    while((linea=buffer.readLine())!=null){
                        //ciclo while que se ejecuta mientras la linea del archivo contenga algún caracter

                        atributosPersona = linea.split(",");//separación de la línea por comas, asignando cada elemento a una posicion del arreglo diferente

                        //asignación de los atributos del cliente obtenidos de la linea leída
                        cedula=Integer.valueOf(atributosPersona[0]);
                        nombre=atributosPersona[1];
                        genero=atributosPersona[2];

                        Persona cliente = new Persona(cedula,nombre,genero);//creación de la persona a agregar

                        listaClientes.add(cliente);//agregar la persona a la lista de clientes registrados
                    }

                    buffer.close();//cierre de buffer de lectura

                } catch (IOException ex) {
                    System.out.println(ex);
                }          

            } catch (FileNotFoundException ex) {
                System.out.println(ex);
            }
        }
        
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroupGender = new javax.swing.ButtonGroup();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txtNombre = new javax.swing.JTextField();
        txtCedula = new javax.swing.JTextField();
        jRadioButtonM = new javax.swing.JRadioButton();
        jRadioButtonF = new javax.swing.JRadioButton();
        btnRegistrar = new javax.swing.JButton();

        jLabel1.setText("Registro personas");

        jLabel2.setText("Cédula:");

        jLabel3.setText("Nombre:");

        jLabel4.setText("Género:");

        txtCedula.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtCedulaKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtCedulaKeyTyped(evt);
            }
        });

        buttonGroupGender.add(jRadioButtonM);
        jRadioButtonM.setText("M");

        buttonGroupGender.add(jRadioButtonF);
        jRadioButtonF.setText("F");

        btnRegistrar.setText("Registrar");
        btnRegistrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRegistrarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(146, 146, 146)
                        .addComponent(jLabel1))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(106, 106, 106)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3)
                            .addComponent(jLabel2)
                            .addComponent(jLabel4))
                        .addGap(27, 27, 27)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jRadioButtonM)
                                .addGap(18, 18, 18)
                                .addComponent(jRadioButtonF))
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(txtCedula, javax.swing.GroupLayout.DEFAULT_SIZE, 85, Short.MAX_VALUE)
                                .addComponent(txtNombre))))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(156, 156, 156)
                        .addComponent(btnRegistrar)))
                .addContainerGap(141, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addGap(43, 43, 43)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2)
                            .addComponent(txtCedula, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(25, 25, 25)
                        .addComponent(jLabel3))
                    .addComponent(txtNombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(jRadioButtonM, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jRadioButtonF))
                .addGap(43, 43, 43)
                .addComponent(btnRegistrar)
                .addContainerGap(66, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    public Persona buscarPersona(int cedula){
        //función que retorna una persona conforme al número de cédula especificado por parámetros
        for(Persona p:listaClientes){
            if(cedula==p.getCedula())
               return p;
        }
        return null;
    }
    
    private void btnRegistrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRegistrarActionPerformed
        //método que maneja la acción del botón de registrar
        
        if((this.txtCedula.getText().equals("")) || (this.txtNombre.getText().equals("") || ((this.jRadioButtonM.isSelected()==false)&&(this.jRadioButtonF.isSelected()==false)) )){
            JOptionPane.showMessageDialog(this,"Todos los campos son obligatorios","ERROR",JOptionPane.ERROR_MESSAGE);
        }else{
            boolean repetido=false;
            long cedula=Long.valueOf(this.txtCedula.getText());

            int cifras= 0;
            long n=cedula;    
            while(n!=0){            
                n = n/10;        
                cifras++;          
            }   

            if(cifras<10){

                for(Persona p:listaClientes){
                    if((int)cedula==p.getCedula())
                        repetido=true;
                }

                if(!repetido){
                    String nombre=this.txtNombre.getText();
                    String genero=this.buttonGroupGender.getSelection().getActionCommand();


                    Persona cliente = new Persona((int)cedula,nombre,genero);
                    listaClientes.add(cliente);
                    limpiarDatos();
                }else{
                    this.txtCedula.setText("");
                    JOptionPane.showMessageDialog(this,"Esta persona ya se encuentra registrada", "Error", JOptionPane.ERROR_MESSAGE);
                }          
            }else{
                this.txtCedula.setText("");
                JOptionPane.showMessageDialog(this,"Digite una cédula válida", "Error", JOptionPane.ERROR_MESSAGE);           
            }
        }
             
    }//GEN-LAST:event_btnRegistrarActionPerformed

    private void txtCedulaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCedulaKeyTyped
        //validación de digitos únicamente en el textField de cédula
        char validar=evt.getKeyChar();
        if (!(Character.isDigit(validar))){
            evt.consume();
        }
    }//GEN-LAST:event_txtCedulaKeyTyped

    private void txtCedulaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCedulaKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtCedulaKeyPressed
    
    public void limpiarDatos(){
        //método para limpiar los textFields del JFrame
        this.txtCedula.setText("");
        this.txtNombre.setText("");        
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnRegistrar;
    public javax.swing.ButtonGroup buttonGroupGender;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    public javax.swing.JRadioButton jRadioButtonF;
    public javax.swing.JRadioButton jRadioButtonM;
    public javax.swing.JTextField txtCedula;
    public javax.swing.JTextField txtNombre;
    // End of variables declaration//GEN-END:variables
}
